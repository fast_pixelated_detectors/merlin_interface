.. _tutorial:

========
Tutorial
========

Test mode
---------

To test the code on a computer without the Merlin software, or without having the Merlin software running:

.. code-block:: python

    >>> from merlin_interface.merlin_interface import MerlinInterface
    >>> mi = MerlinInterface(tcp_ip="127.0.0.1", tcp_port=6341, test_mode=True)
    >>> mi.acquisitiontime
    b'MPX,0000000020,GET,ACQUISITIONTIME,0'
    b'MPX,0000000020,GET,ACQUISITIONTIME,0'

In test_mode, the string which would be sent to the Merlin software is printed to the terminal instead.

The wrapper works either through properties (like acquisitiontime) or through calling the class methods.
Values are set through the properties:

.. code-block:: python

    >>> mi.acquisitiontime = 50
    b'MPX,0000000023,SET,ACQUISITIONTIME,50,0'

An example of calling a class method:

.. code-block:: python

    >>> mi.startacquisition()
    b'MPX,0000000021,CMD,STARTACQUISITION,0'

Non-test mode
-------------

To run the wrapper on a computer with the Merlin software running.

.. code-block:: python

    >>> from merlin_interface.merlin_interface import MerlinInterface
    >>> mi = MerlinInterface(tcp_ip="127.0.0.1", tcp_port=6341)
    >>> mi.acquisitiontime # doctest: +SKIP

Note, this will not work properly without the Merlin software running.


Testing tools
-------------

Some of the functionally can be tested with the testing tools:

.. code-block:: python

    >>> from merlin_interface.testing_tools import TestMerlinConn
    >>> import time
    >>> mt = TestMerlinConn()
    >>> mt.start_command_listening()
    >>> time.sleep(0.01)
    >>> from merlin_interface.merlin_interface import MerlinInterface
    >>> mi = MerlinInterface(tcp_ip=mt.ip.value, tcp_port=mt.port.value)
    >>> mi.acquisitiontime
    b'MPX,0000000020,GET,ACQUISITIONTIME,0'
    >>> mt.stop_socket()
