.. _related_projects:

================
Related projects
================

- `fpd_live_imaging <https://fast_pixelated_detectors.gitlab.io/fpd_live_imaging/>`_: for doing live processing and imaging of data acquired on a fast pixelated detector using a scanning transmission electron microscope.
- `pixStem <http://pixstem.org>`_: post processing of pixelated STEM data.
