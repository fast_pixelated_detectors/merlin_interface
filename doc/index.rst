Welcome to Merlin interface's documentation!
==================================================

News
----

**2024-10-27: merlin_interface 0.0.6 released!**

* Add several commands for the STEM / scan mode: ``startrecord``, ``startsecord``, ``scanx``, ``scany`` and several others.
* Add ``startlive``, and ``fileformat`` which can be used to set raw mode.
* Switch from ``setup.py`` to ``pyproject.toml``, and switch to black style checker, and ruff linter.


*2020-1-28: merlin_interface 0.0.5 released!*

* Fixed ``continuousrw``, ``acquisitiontime`` and ``acquisitionperiod``, which was broken due to changes in the Merlin software API.
* Improve responsiveness, by not making a new connection every time a command is executed.

About Merlin interface
----------------------

Python library for interfacing with a Medipix3 detector through the Merlin readout software, over the TCP/IP interface.

The source code is accessible at https://gitlab.com/fast_pixelated_detectors/merlin_interface.

Contributions, feature requests and bug reports are welcome on the `issues page <https://gitlab.com/fast_pixelated_detectors/merlin_interface/issues>`_.

.. image:: images/index/merlin_interface_example.png
    :scale: 99 %
    :align: center


.. toctree::
   install
   tutorial
   api_documentation
   related_projects
   :maxdepth: 2
   :caption: Contents:

Initial development of merlin_interface by Magnus Nord funded by epsrc via the project "fast pixel detectors: a paradigm shift in stem imaging" (grant reference ep/m009963/1).
Current development by Magnus Nord funded via the European Union’s Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No 838001.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
