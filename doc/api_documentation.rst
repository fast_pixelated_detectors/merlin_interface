.. _api:

=================
API documentation
=================

MerlinInterface
=====================
.. autoclass:: merlin_interface.merlin_interface.MerlinInterface
    :members:
    :undoc-members:


TestMerlinConn
===============
.. autoclass:: merlin_interface.testing_tools.TestMerlinConn
    :members:
    :undoc-members:

