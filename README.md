### Webpage: https://fast_pixelated_detectors.gitlab.io/merlin_interface/

Development webpage: https://gitlab.com/fast_pixelated_detectors/merlin_interface/builds/artifacts/master/file/public_development/index.html?job=pages_development

# Merlin interface

Simple python wrapper for interfacing with the Merlin Medipix readout software through the TCP/IP API.
